extends KinematicBody2D

const JUMP_BUTTON: String = 'jump'
const LEFT_BUTTON: String = 'left'
const RIGHT_BUTTON: String = 'right'
const ATTACK_BUTTON: String = 'attack'
const GRAVITY: int = 500

var run_speed: int = 350
var velocity: Vector2 = Vector2(0,0)

func apply_gravity():
	if is_on_floor():
		velocity.y = 0
	else:
		velocity.y += GRAVITY

func _physics_process(delta):
	apply_gravity()
	velocity = move_and_slide(velocity)
