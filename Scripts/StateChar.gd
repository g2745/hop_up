extends KinematicBody2D

onready var state_machine = $AnimationTree.get("parameters/playback")
onready var char_sprite = $Sprite
onready var char_collision = $CollisionShape2D
onready var char_left_ray_cast = $LeftWallRayCast
onready var char_right_ray_cast = $RightWallRayCast

var velocity = Vector2.ZERO
var jump_counter = 0
var jump_max = 1

const RUN_SPEED = 350
const GRAVITY: int = 150
const JUMP_SPEED: int = 1500
const UP_DIRECTION: Vector2 = Vector2(0, -1)

func _physics_process(delta):
	apply_gravity()
	get_input()
	# jump()
	double_jump()
	# wall_jump()
	# wall_slide()
	wall_jump_with_slide()
	velocity = move_and_slide(velocity, UP_DIRECTION)

func get_input():
	# var currentState = state_machine.get_current_node()
	# velocity = Vector2.ZERO

	if Input.is_action_just_pressed("ui_attack"):
		state_machine.travel("Attack")
		return

	if Input.is_action_pressed("ui_left"):
		velocity.x = -RUN_SPEED
		flip_char(true)
	elif Input.is_action_pressed("ui_right"):
		velocity.x = RUN_SPEED
		flip_char(false)
	else:
		velocity.x = 0

	# velocity = velocity.normalized() * run_speed

	if velocity.length() > 0:
		state_machine.travel("Run")

	if velocity.x == 0:
		state_machine.travel("Idle")

func apply_gravity():
	if is_on_floor() and velocity.y > 0:
		velocity.y = 0
	else:
		velocity.y += GRAVITY 

func jump():
	if Input.is_action_just_pressed("ui_jump"):
		velocity.y -= JUMP_SPEED
		state_machine.travel("Jump")

func double_jump():
	if Input.is_action_just_pressed("ui_jump"):
		if jump_counter < jump_max:
			state_machine.travel("Jump")
			velocity.y = -JUMP_SPEED
			jump_counter += 1

	if is_on_floor():
		jump_counter = 0

func wall_jump():
	if Input.is_action_just_pressed("ui_jump"):
		if not is_on_floor() and next_to_right_wall():
			print('next to right wall')
			velocity.x -= 1500
			jump_counter = 0
		if not is_on_floor() and next_to_left_wall():
			print('next to left wall')
			velocity.x += 1500
			jump_counter = 0

func wall_slide():
	if not is_on_floor() and next_to_wall() and velocity.y > 0:
		velocity.y = 100

		state_machine.travel("WallSlide")
		if next_to_right_wall():
			flip_char(true)
		elif next_to_left_wall():
			flip_char(false)
		else:
			state_machine.travel("Fall")
	
func wall_jump_with_slide():
	wall_jump()
	wall_slide()

func next_to_wall():
	return next_to_right_wall() or next_to_left_wall()

func next_to_right_wall():
	return $RightWallRayCast.is_colliding()

func next_to_left_wall():
	return $LeftWallRayCast.is_colliding()


## helpers
func flip_char(flip: bool):
	if flip:
		char_sprite.flip_h = true
		char_collision.position.x = 20
		char_right_ray_cast.position.x = 20
		char_left_ray_cast.position.x = 20
	else:
		char_sprite.flip_h = false
		char_collision.position.x = 0
		char_right_ray_cast.position.x = 0
		char_left_ray_cast.position.x = 0


func _on_WeaponHit_area_entered(area:Area2D):
	if area.is_in_group('hurtbox'):
		area.take_damage()
		
