extends StateMachine

func _state_logic(delta):
	add_state("idle")
	add_state("run")
	add_state("attack")
	call_deferred("set_state", states.idle)

func _get_transition(delta):
	parent._apply_grafity(delta)

func _enter_state(new_state, old_state):
	pass

func _exit_state(old_state, new_state):
	pass
