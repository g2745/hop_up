extends KinematicBody2D

const UP: Vector2  = Vector2(0, -1)
const GRAVITY: int = 1200
const SLOPE_STOP: int = 64

var velocity: Vector2 = Vector2()
var move_speed: int = 5 * 80
var jump_velocity: int = -720
var is_grounded: bool = false

onready var raycasts_grounded = $RayCastsGrounded
onready var anim_player = $AnimationPlayer

func _apply_grafity(delta):
	velocity.y += GRAVITY * delta

func _physics_process(delta):
	_get_input()
	_apply_grafity(delta)	
	velocity = move_and_slide(velocity, UP, SLOPE_STOP)
	# is_grounded = is_on_floor()
	is_grounded = _check_is_grounded()
	_assign_animation()

func _input(event):
	if event.is_action_pressed("ui_jump") && is_grounded:
		velocity.y = jump_velocity

func _get_input():
	var move_direction = -int(Input.is_action_pressed("ui_left")) + int(Input.is_action_pressed("ui_right"))
	velocity.x = lerp(velocity.x, move_speed * move_direction, _get_h_weight())

	if move_direction != 0:
	# 	self.scale.x = move_direction
		$Sprite.flip_h = false
		if move_direction == -1:
			$Sprite.flip_h = true

# so player has less controll in the air
func _get_h_weight():
	return 0.2 if is_grounded else 0.1

func _check_is_grounded():
	for raycast in raycasts_grounded.get_children():
		if raycast.is_colliding():
			return true

	return false

func _assign_animation():
	var anim: String = "Idle"

	if !is_grounded:
		anim = "Jump"
	elif velocity.x != 0:
		anim = "Run"

	if anim_player.assigned_animation != anim:
		anim_player.play(anim)
