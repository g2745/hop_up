extends Area2D

var hits: int = 3

func take_damage():
	if hits-1 == 0:
		get_parent().queue_free()

	hits -= 1
