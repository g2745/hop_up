extends KinematicBody2D

enum {
	IDLE,
	MOVE_LEFT,
	MOVE_RIGHT,
	JUMP,
	ATTACK
}

onready var move_animation_player = $MoveAnimationPlayer 
onready var move_animation_tree = $MoveAnimationTree
onready var move_animation_state = move_animation_tree.get("parameters/playback")
onready var attack_animation_player = $AttackAnimationPlayer

const RUN_SPEED: int = 350
const JUMP_SPEED: int = 1500
const GRAVITY: float = 150.0
const UP_DIRECTION: Vector2 = Vector2(0, -1)

const MOVE_RIGHT_BUTTON: String = "ui_right"
const MOVE_LEFT_BUTTON: String = "ui_left"
const JUMP_BUTTON: String = "ui_jump"
const ATTACK_BUTTON: String = "ui_attack"

var velocity: Vector2 = Vector2.ZERO
var state = MOVE_RIGHT

var jump_counter: int = 0
var jump_max: int = 2

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	apply_gravity()
	move_state()

func apply_gravity():
	if is_on_floor() and velocity.y > 0:
		velocity.y = 0
	else:
		velocity.y += GRAVITY

# ***************************** MOVEMENTS ***************************** #
func _button_pressed(button: String):
	return Input.is_action_pressed(button)

func _button_just_pressed(button: String):
	return Input.is_action_just_pressed(button)

func move_state():
	if is_on_floor():
		jump_counter = 0

	if _button_pressed(MOVE_LEFT_BUTTON):
		if not state == ATTACK:
			state = MOVE_LEFT
			move_animation_player.play("RunLeft")
		velocity.x = -RUN_SPEED
	elif _button_pressed(MOVE_RIGHT_BUTTON):
		if not state == ATTACK:
			state = MOVE_RIGHT
			move_animation_player.play("RunRight")
		velocity.x = RUN_SPEED
	else:
		stay_still()

	if _button_just_pressed(JUMP_BUTTON):
		if jump_counter < jump_max:
			state = JUMP
			move_animation_player.play("JumpLeft")
			velocity.y = -JUMP_SPEED
			jump_counter += 1

	if _button_just_pressed(ATTACK_BUTTON):
		if state == MOVE_LEFT:
			attack_animation_player.play("AttackLeft")
		elif state == MOVE_RIGHT:
			attack_animation_player.play("AttackRight")
		state = ATTACK

	velocity = move_and_slide(velocity, UP_DIRECTION)

func stay_still():
	if state == JUMP or state == ATTACK:
		return

	if state == MOVE_LEFT:
		move_animation_player.play("IdleLeft")
	elif state == MOVE_RIGHT:
		move_animation_player.play("IdleRight")

	velocity.x = 0

func _calc_x_input_strength():
	var right_strength = Input.get_action_strength("right")
	var left_strength = Input.get_action_strength("left")
	
	return right_strength - left_strength

func move_tree_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = _calc_x_input_strength()
	input_vector = input_vector.normalized()

	if input_vector != Vector2.ZERO:
		move_animation_tree.set("parameters/Idle/blend_position", input_vector)
		move_animation_tree.set("parameters/Run/blend_position", input_vector)
		move_animation_state.travel("Run")
		velocity = velocity.move_toward(input_vector * 100, 500 * delta)
	else:
		move_animation_state.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, 1000 * delta)
# ***************************** MOVEMENTS ***************************** #

func _attack_animation_finished():
	state = IDLE
