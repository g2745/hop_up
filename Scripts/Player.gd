extends KinematicBody2D

enum {
	MOVED_STOP,
	MOVED_LEFT,
	MOVED_RIGHT
}

var moved_state = MOVED_RIGHT

onready var move_animation_sprite = $MoveAnimatedSprite
onready var attack_animation_player = $AttackAnimationPlayer
onready var move_animation_player = $MoveAnimationPlayer
onready var animationTree = $MoveAnimationTree
onready var animationState = animationTree.get("parameters/playback")

var velocity: Vector2 = Vector2(0, 0)

export (int) var WALK_SPEED: int = 350
export (int) var JUMP_SPEED: int = 1500

const GRAVITY: float = 150.0
const UP_DIRECTION: Vector2 = Vector2(0, -1)

var jump_count: int = 0
var jump_max: int = 1

var jump_wall_count: int = 2
var jump_wall_up: int = 60 # jump hight on the y axis
var jump_from_wall: int = 500 # push back from the wall on x axis

const JUMP_BUTTON: String = 'jump'
const LEFT_BUTTON: String = 'left'
const RIGHT_BUTTON: String = 'right'
const ATTACK_BUTTON: String = 'attack'

var can_walk_left: bool = true
var can_walk_right: bool = true

var animation_is_active: bool = false

func _ready():
	# $Sprite.hide()
	animationTree.active = true
	pass

func activate_animation(state: bool):
	print('getting here: %s' % state)
	animation_is_active = state

func _stop_physics_for_one_frame():
	yield(get_tree(), 'idle_frame')

func _play_animation(animation_name: String):
	# move_animation_sprite.play(animation_name)
	move_animation_player.play(animation_name)

func _flip_char_to_the_right():
	move_animation_sprite.flip_h = false

func _flip_char_to_the_left():
	move_animation_sprite.flip_h = true

func _move_right():
	velocity.x = WALK_SPEED

func _move_left():
	velocity.x = -WALK_SPEED

func _move_stop():
	velocity.x = 0

func _attack_pressed():
	return Input.is_action_just_pressed(ATTACK_BUTTON)

func _move_left_pressed():
	return Input.is_action_pressed(LEFT_BUTTON)

func _move_right_pressed():
	return Input.is_action_pressed(RIGHT_BUTTON)

func _jump_pressed():
	return Input.is_action_just_pressed(JUMP_BUTTON)

func _jump_reset():
	jump_count = 0

func _walk_left():
	moved_state = MOVED_LEFT
	# _play_animation('RunLeft')
	# _flip_char_to_the_left()
	_play_animation("RunLeft")
	_move_left()

func _walk_right():
	moved_state = MOVED_RIGHT
	# _play_animation('walk')
	# _flip_char_to_the_right()
	_play_animation("RunRight")
	_move_right()

func _stand_still():
	# _play_animation('idle')
	if moved_state == MOVED_RIGHT:
		_play_animation("IdleRight")
	elif moved_state == MOVED_LEFT:
		_play_animation("IdleLeft")

	_move_stop()
	# moved_state = MOVED_STOP

func _move():
	if _move_left_pressed() and _move_right_pressed():
		_stand_still()
	elif _move_left_pressed():
		if can_walk_left:
			_walk_left()
	elif _move_right_pressed():
		if can_walk_right:
			_walk_right()
	elif not is_on_floor():
		_play_animation('jump')
	else:
		_stand_still()
	
func _calc_x_input_strength():
	var right_strength = Input.get_action_strength("right")
	var left_strength = Input.get_action_strength("left")

	return right_strength - left_strength

func move_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = _calc_x_input_strength()
	input_vector = input_vector.normalized()

	# animationTree.set("parameters/Jump/blend_position", input_vector)

	if input_vector != Vector2.ZERO:
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Run/blend_position", input_vector)
		animationState.travel("Run")
		velocity = velocity.move_toward(input_vector * WALK_SPEED, 500 * delta)
	else:
		animationState.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, 1000 * delta)


func _apply_gravity():
	if is_on_floor() and velocity.y > 0:
		velocity.y = 0
	else:
		velocity.y += GRAVITY

func _jump():
	if _jump_pressed() and is_on_floor():
		velocity.y = -JUMP_SPEED

func _double_jump():
	if _jump_pressed() and jump_count < jump_max:
		velocity.y = -JUMP_SPEED
		jump_count += 1

	if is_on_floor():
		_jump_reset()

func _wall_jump():
	jump_wall_count = 2
	if _jump_pressed() and jump_wall_count > 0:
		velocity.y = -JUMP_SPEED
		jump_wall_count -= 1
		_jump_reset()

		if not is_on_floor() and _next_to_right_wall():
			velocity.x -= jump_from_wall
			velocity.y -= jump_wall_up

			can_walk_right = false
			yield(get_tree().create_timer(0.1), "timeout")
			can_walk_right = true

		if not is_on_floor() and _next_to_left_wall():
			velocity.x += jump_from_wall
			velocity.y -= jump_wall_up

			can_walk_left = false
			yield(get_tree().create_timer(0.1), "timeout")
			can_walk_left = true

func _wall_jump_with_slide():
	if is_on_floor() or _next_to_wall():
		_wall_jump()
		_wall_slide()

func _wall_slide():
	if _next_to_wall() and velocity.y > 150:
		velocity.y = 100
		if _next_to_right_wall():
			_flip_char_to_the_left()
		if _next_to_left_wall():
			_flip_char_to_the_right()

func _next_to_wall():
	return _next_to_right_wall() or _next_to_left_wall()

func _next_to_right_wall():
	return $RightWall.is_colliding()

func _next_to_left_wall():
	return $LeftWall.is_colliding()

func _physics_process(delta):
	_apply_gravity()
	# _attack()
	# _move()
	move_state(delta)
	# _jump()
	_double_jump()
	# _wall_jump()
	# _wall_jump_with_slide()

	velocity = move_and_slide(velocity, UP_DIRECTION)

func _attack():
	if _attack_pressed():
		# $Sprite.show()
		# move_animation_sprite.hide()
		# move_animation_player.queue_free()
		attack_animation_player.play('AttackRight')

# is triggered after AttackAnimation is finished in AnimationPlayer
func attack_animation_finished():
	pass
	# $Sprite.hide()
	# move_animation_sprite.show()
